# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile



-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule


-dontwarn com.squareup.**
-keep class com.squareup.picasso.** { *; }
-keepclasseswithmembers class * {
    @com.squareup.picasso.** *;
}
-keepclassmembers class * {
    @com.squareup.picasso.** *;
}

-dontwarn org.greenrobot.eventbus.**
-keep class org.greenrobot.eventbus.** { *; }
-keepclasseswithmembers class * {
    @org.greenrobot.eventbus.** *;
}
-keepclassmembers class * {
    @org.greenrobot.eventbus.** *;
}


-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.** { *; }
-keepclasseswithmembers class * {
    @com.google.android.gms.** *;
}
-keepclassmembers class * {
    @com.google.android.gms.** *;
}

-keep class com.gmail.sfcodeapps.registrationplatesofeurope** { *; }
-dontwarn com.gmail.sfcodeapps.registrationplatesofeurope**

-keep class cn.pedant.sweetalert.**
-keep class com.github.SandroMachado.**
-keep class com.google.code.gson.**

-keep class cn.pedant.SweetAlert.Rotate3dAnimation {
  public <init>(...);
}

-dontshrink
-dontoptimize
-dontpreverify

-keep class org.openalpr.OpenALPR**
-keep class org.openalpr.model.Results**

-keep class org.openalpr.** { *; }

-keepclasseswithmembers class * {
    @org.openalpr.** *;
}
-keepclassmembers class * {
    @org.openalpr.** *;
}


-keep class android.widget**

-keepattributes Signature

-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer