package com.gmail.sfcodeapps.europlates.helper;

import android.content.Context;

import com.gmail.sfcodeapps.europlates.R;
import com.gmail.sfcodeapps.europlates.registrationPlates.CountryItem;

import java.util.ArrayList;
import java.util.HashMap;

public class Values {

    Context context;

    public Values(Context context){
        this.context = context;
    }

 /*   private HashMap<String, String> countries;
    private HashMap<String, String> importantCities;
    public ArrayList<CountryItem> countryItems;  */


    public HashMap<String,String> generateCountries(){
        HashMap<String,String> countries = new HashMap<>();
        countries.put("A", context.getString(R.string.austria));
        countries.put("BG", context.getString(R.string.bulgaria));
        countries.put("CH", context.getString(R.string.switzerland));
        countries.put("CZ", context.getString(R.string.czech));
        countries.put("D", context.getString(R.string.germany));
        countries.put("GB", context.getString(R.string.uk));
        countries.put("HR", context.getString(R.string.croatia));
        countries.put("I", context.getString(R.string.italy));
        countries.put("MK", context.getString(R.string.macedonia));
        countries.put("MNE", context.getString(R.string.montenegro));
        countries.put("PL", context.getString(R.string.poland));
        countries.put("RO", context.getString(R.string.romania));
        countries.put("RUS", context.getString(R.string.russia));
        countries.put("SK", context.getString(R.string.slovakia));
        countries.put("SLO", context.getString(R.string.slovenia));
        countries.put("SRB", context.getString(R.string.serbia));
        countries.put("UA", context.getString(R.string.ukraine));
        return countries;
    }

    public HashMap<String,String> generateImportantCities(){
        HashMap<String,String> importantCities = new HashMap<>();
        importantCities.put("Vienna", context.getString(R.string.vienna));
        importantCities.put("Sofia", context.getString(R.string.sofia));
        importantCities.put("Zürich", context.getString(R.string.zurich));
        importantCities.put("Praha", context.getString(R.string.prague));
        importantCities.put("Berlin", context.getString(R.string.berlin));
        importantCities.put("London", context.getString(R.string.london));
        importantCities.put("Zagreb", context.getString(R.string.zagreb));
        importantCities.put("Roma", context.getString(R.string.roma));
        importantCities.put("Skopje", context.getString(R.string.skopje));
        importantCities.put("Podgorica", context.getString(R.string.podgorica));
        importantCities.put("Bucharest", context.getString(R.string.bucharest));
        importantCities.put("Ljubljana", context.getString(R.string.ljubljana));
        importantCities.put("Beograd", context.getString(R.string.belgrade));
        importantCities.put("Kiev", context.getString(R.string.kiev));
        importantCities.put("Bratislava",context.getString(R.string.bratislava));
        importantCities.put("Bern",context.getString(R.string.bern));
        importantCities.put("Moscow",context.getString(R.string.moscow));

        importantCities.put("Vienna (district)",context.getString(R.string.vienna_district));
        importantCities.put("Sofia (district)",context.getString(R.string.sofia_district));
        importantCities.put("Praha (district)",context.getString(R.string.prague_district));
        importantCities.put("Kiev (district)",context.getString(R.string.kiev_district));
        importantCities.put("London (Wimbledon)",context.getString(R.string.london_wimbledon));
        importantCities.put("London (Stanmore)",context.getString(R.string.london_stanmore));
        importantCities.put("London (Sidcup)",context.getString(R.string.london_sidcup));
        importantCities.put("Moscow Oblast",context.getString(R.string.moscow_oblast));
        importantCities.put("Warszawa (Białołęka)",context.getString(R.string.warsaw_bialoleka));
        importantCities.put("Warszawa (Bemowo)",context.getString(R.string.warsaw_bemowo));
        importantCities.put("Warszawa (Bielany)",context.getString(R.string.warsaw_bielany));
        importantCities.put("Warszawa (Mokotów)",context.getString(R.string.warsaw_mokotow));
        importantCities.put("Warszawa (Praga Południe)",context.getString(R.string.warsaw_praga_poludnie));
        importantCities.put("Warszawa (Praga Północ)",context.getString(R.string.warsaw_praga_polnoc));
        importantCities.put("Warszawa (Śródmieście)",context.getString(R.string.warsaw_srodmiescie));
        importantCities.put("Warszawa (Targówek)",context.getString(R.string.warsaw_targowek));
        importantCities.put("Warszawa (Ursus)",context.getString(R.string.warsaw_ursus));
        importantCities.put("Warszawa (Ursynów)",context.getString(R.string.warsaw_ursynow));
        importantCities.put("Warszawa (Wawer)",context.getString(R.string.warsaw_wawer));
        importantCities.put("Warszawa (Ochota)",context.getString(R.string.warsaw_ochota));
        importantCities.put("Warszawa (Włochy)",context.getString(R.string.warsaw_wlochy));
        importantCities.put("Warszawa (Wilanów)",context.getString(R.string.warsaw_wilanow));
        importantCities.put("Warszawa (Rembertów)",context.getString(R.string.warsaw_rembertow));
        importantCities.put("Warszawa (Żoliborz)",context.getString(R.string.warsaw_zoliborz));
        importantCities.put("Warszawa (Wesoła)",context.getString(R.string.warsaw_wesola));
        importantCities.put("Warszawa (Wola)",context.getString(R.string.warsaw_wola));


        importantCities.put("Graz",context.getString(R.string.graz));
        importantCities.put("Klagenfurt",context.getString(R.string.klagenfurt));
        importantCities.put("Linz",context.getString(R.string.linz));
        importantCities.put("Salzburg",context.getString(R.string.salzburg));
        importantCities.put("Innsbruck",context.getString(R.string.innsbruck));
        importantCities.put("Plovdiv",context.getString(R.string.plovdiv));
        importantCities.put("Varna",context.getString(R.string.varna));
        importantCities.put("Burgas",context.getString(R.string.burgas));
        importantCities.put("Split",context.getString(R.string.split));
        importantCities.put("Dubrovnik",context.getString(R.string.dubrovnik));
        importantCities.put("Osijek",context.getString(R.string.osijek));
        importantCities.put("Rijeka",context.getString(R.string.rijeka));
        importantCities.put("Brno",context.getString(R.string.brno));
        importantCities.put("Ostrava",context.getString(R.string.ostrava));
        importantCities.put("Plzeň",context.getString(R.string.plzen));
        importantCities.put("Hansestadt Hamburg",context.getString(R.string.hamburg));
        importantCities.put("Munich",context.getString(R.string.munich));
        importantCities.put("Frankfurt am Main",context.getString(R.string.frankfurt));
        importantCities.put("Stuttgart",context.getString(R.string.stuttgart));
        importantCities.put("Cologne",context.getString(R.string.cologne));
        importantCities.put("Dortmund",context.getString(R.string.dortmund));
        importantCities.put("Düsseldorf",context.getString(R.string.dusseldorf));
        importantCities.put("Essen",context.getString(R.string.essen));
        importantCities.put("Leipzig",context.getString(R.string.leipzig));
        importantCities.put("Bologna",context.getString(R.string.bologna));
        importantCities.put("Firenze",context.getString(R.string.firenze));
        importantCities.put("Genova",context.getString(R.string.genova));
        importantCities.put("Milano",context.getString(R.string.milano));
        importantCities.put("Napoli",context.getString(R.string.napoli));
        importantCities.put("Venezia",context.getString(R.string.venezia));
        importantCities.put("Bari",context.getString(R.string.bari));
        importantCities.put("Palermo",context.getString(R.string.palermo));
        importantCities.put("Budva",context.getString(R.string.budva));
        importantCities.put("Herceg Novi",context.getString(R.string.herceg_novi));
        importantCities.put("Kraków",context.getString(R.string.krakow));
        importantCities.put("Łódź",context.getString(R.string.lodz));
        importantCities.put("Wrocław",context.getString(R.string.wroclaw));
        importantCities.put("Gdańsk",context.getString(R.string.gdansk));
        importantCities.put("Poznań",context.getString(R.string.poznan));
        importantCities.put("Szczecin",context.getString(R.string.szczecin));
        importantCities.put("Niš",context.getString(R.string.nis));
        importantCities.put("Novi Sad",context.getString(R.string.novi_sad));
        importantCities.put("Geneva",context.getString(R.string.geneva));
        importantCities.put("Lucerne",context.getString(R.string.lucerne));
        importantCities.put("Basel-Stadt",context.getString(R.string.basel));
        importantCities.put("Manchester",context.getString(R.string.manchester));
        importantCities.put("Birmingham",context.getString(R.string.birmingham));
        importantCities.put("Glasgow",context.getString(R.string.glasgow));
        importantCities.put("Edinburgh",context.getString(R.string.edinburgh));
        importantCities.put("Lviv",context.getString(R.string.lviv));
        importantCities.put("Kharkiv",context.getString(R.string.kharkiv));
        importantCities.put("Dnipropetrovsk",context.getString(R.string.dnipropetrovsk));
        importantCities.put("Odessa",context.getString(R.string.odessa));
        importantCities.put("Donetsk",context.getString(R.string.donetsk));

        importantCities.put("Republic of Adygea",context.getString(R.string.adygea));
        importantCities.put("Republic of Bashkortostan",context.getString(R.string.bashkortostan));
        importantCities.put("Republic of Buryatia",context.getString(R.string.buryatia));
        importantCities.put("Republic of Dagestan",context.getString(R.string.dagestan));
        importantCities.put("Republic of Ingushetia",context.getString(R.string.ingushetia));
        importantCities.put("Republic of Kalmykia",context.getString(R.string.kalmykia));
        importantCities.put("Republic of Karelia",context.getString(R.string.karelia));
        importantCities.put("Republic of Mordovia",context.getString(R.string.mordovia));
        importantCities.put("Republic of North Ossetia–Alania",context.getString(R.string.north_ossetia_alania));
        importantCities.put("Republic of Tatarstan",context.getString(R.string.tatarstan));
        importantCities.put("Republic of Khakassia",context.getString(R.string.khakassia));
        importantCities.put("Chechen Republic",context.getString(R.string.checen_republic));
        importantCities.put("Republic of Crimea",context.getString(R.string.crimea));

        importantCities.put("Graz (district)",context.getString(R.string.graz_district));
        importantCities.put("Klagenfurt (district)",context.getString(R.string.klagenfurt_district));
        importantCities.put("Linz (district)",context.getString(R.string.linz_district));
        importantCities.put("Salzburg (district)",context.getString(R.string.salzburg_district));
        importantCities.put("Innsbruck (district)",context.getString(R.string.innsbruck_district));
        importantCities.put("Kraków (district)",context.getString(R.string.krakow_district));
        importantCities.put("Łódź (district)",context.getString(R.string.lodz_district));
        importantCities.put("Poznań (district)",context.getString(R.string.poznan_district));
        importantCities.put("Wrocław (district)",context.getString(R.string.wroclaw_district));

        return importantCities;
    }

    public ArrayList<CountryItem> generateCountryItems(){
        ArrayList<CountryItem> countryItems = new ArrayList<>();

        countryItems.add(new CountryItem("(A) " + context.getString(R.string.austria),R.drawable.austria,R.drawable.regaustria,"A"));
        countryItems.add(new CountryItem("(BG) " + context.getString(R.string.bulgaria),R.drawable.bulgaria,R.drawable.regbulgaria,"BG"));
        countryItems.add(new CountryItem("(CH) " + context.getString(R.string.switzerland),R.drawable.switzerland,R.drawable.regswitzerland,"CH"));
        countryItems.add(new CountryItem("(CZ) " + context.getString(R.string.czech),R.drawable.czech,R.drawable.regczech,"CZ"));
        countryItems.add(new CountryItem("(D) " + context.getString(R.string.germany),R.drawable.germany,R.drawable.reggermany,"D"));
        countryItems.add(new CountryItem("(GB) " + context.getString(R.string.uk),R.drawable.uk,R.drawable.reguk,"GB"));
        countryItems.add(new CountryItem("(HR) " + context.getString(R.string.croatia),R.drawable.croatia,R.drawable.regcroatia,"HR"));
        countryItems.add(new CountryItem("(I) " + context.getString(R.string.italy),R.drawable.italy,R.drawable.regitaly,"I"));
        countryItems.add(new CountryItem("(MK) " + context.getString(R.string.macedonia),R.drawable.macedonia,R.drawable.regmacedonia,"MK"));
        countryItems.add(new CountryItem("(MNE) " + context.getString(R.string.montenegro),R.drawable.montenegro,R.drawable.regmontenegro,"MNE"));
        countryItems.add(new CountryItem("(PL) " + context.getString(R.string.poland),R.drawable.poland,R.drawable.regpoland,"PL"));
        countryItems.add(new CountryItem("(RO) " + context.getString(R.string.romania),R.drawable.romania,R.drawable.regromania,"RO"));
        countryItems.add(new CountryItem("(RUS) " + context.getString(R.string.russia),R.drawable.russia,R.drawable.regrussia,"RUS"));
        countryItems.add(new CountryItem("(SK) " + context.getString(R.string.slovakia),R.drawable.slovakia,R.drawable.regslovakia,"SK"));
        countryItems.add(new CountryItem("(SLO) " + context.getString(R.string.slovenia),R.drawable.slovenia,R.drawable.regslovenia,"SLO"));
        countryItems.add(new CountryItem("(SRB) " + context.getString(R.string.serbia),R.drawable.serbia,R.drawable.regserbia,"SRB"));
        countryItems.add(new CountryItem("(UA) " + context.getString(R.string.ukraine),R.drawable.ukraine,R.drawable.regukraine,"UA"));
        return countryItems;
    }


    public ArrayList<Integer> generateCodeMaps(){
        ArrayList<Integer> codeMaps = new ArrayList<>();
        codeMaps.add(R.drawable.mapbulgaria);
        codeMaps.add(R.drawable.mapczech);
        codeMaps.add(R.drawable.mapgermany);
        codeMaps.add(R.drawable.mapmacedonia);
        codeMaps.add(R.drawable.mapmontenegro);
        codeMaps.add(R.drawable.mappoland);
        codeMaps.add(R.drawable.mapromania);
        codeMaps.add(R.drawable.maprussia);
        codeMaps.add(R.drawable.mapserbia);
        codeMaps.add(R.drawable.mapslovakia);
        codeMaps.add(R.drawable.mapukraine);
        return codeMaps;
    }

    public ArrayList<String> generateCodeMapsCountries(){
        ArrayList<String> countries = new ArrayList<>();
        countries.add(context.getString(R.string.bulgaria));
        countries.add(context.getString(R.string.czech));
        countries.add(context.getString(R.string.germany));
        countries.add(context.getString(R.string.macedonia));
        countries.add(context.getString(R.string.montenegro));
        countries.add(context.getString(R.string.poland));
        countries.add(context.getString(R.string.romania));
        countries.add(context.getString(R.string.russia));
        countries.add(context.getString(R.string.serbia));
        countries.add(context.getString(R.string.slovakia));
        countries.add(context.getString(R.string.ukraine));
        return countries;
    }

    public ArrayList<String> generateAboutTitles(){
        ArrayList<String> titles = new ArrayList<>();
        titles.add(context.getString(R.string.version));
        titles.add(context.getString(R.string.supported_languages));
        titles.add(context.getString(R.string.info_sources));
        titles.add(context.getString(R.string.image_sources));
        titles.add(context.getString(R.string.contact));
        titles.add(context.getString(R.string.rate_app));
        return titles;
    }

    public ArrayList<String> generateAboutDescriptions(){
        ArrayList<String> descriptions = new ArrayList<>();
        descriptions.add("1.0.0");
        descriptions.add(context.getString(R.string.supported_languages_text));
        descriptions.add(context.getString(R.string.info_sources_text));
        descriptions.add(context.getString(R.string.image_sources_text));
        descriptions.add(context.getString(R.string.contact_text));
        descriptions.add(context.getString(R.string.rate_description));
        return descriptions;
    }

}
