package com.gmail.sfcodeapps.europlates;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.gmail.sfcodeapps.europlates.adapters.HomeListAdapter;
import com.gmail.sfcodeapps.europlates.helper.Values;
import com.gmail.sfcodeapps.europlates.registrationPlates.CountryItem;
import java.util.ArrayList;


/*
COUNTRIES SUPPORTED:
   (A) Austria
   (BG) Bulgaria
   (CH) Switzerland
   (CZ) Czech Republic
   (D) Germany
   (GB) UK
   (HR) Croatia
   (I) Italy
   (MK) Macedonia
   (MNE) Montenegro
   (PL) Poland
   (RO) Romania
   (RUS) Russia
   (SK) Slovakia
   (SLO) Slovenia
   (SRB) Serbia
   (UA) Ukraine

ALSO ADD:
(AM) Armenia
(AZ) Azerbaijan
(TR) Turkey
*/

//TODO design (create your own plate)
//TODO fix recognition list (district) , add translations and recognition for Czech
//TODO fix map positioning for some regions (specifically in Russia)
//TODO fix instructions for Italy
//TODO fix taking photos and removing them from gallery
//TODO exclude Russia,Czech and similar countries from automatic recognition
/*
Amstetten,Austria
Feldkirchen,Austria
Feldkirch
Gmund
Horn
Krems
*/

//TODO LAUNCH CHECKLIST:
//TODO 1. Remove all Logs and android:debuggable
//TODO 2. Remove API keys from comments and store them somewhere
//TODO 3. Prepare End User License Agreement (EULA)
//TODO 4. Delete unused images and resources
//TODO 5. Translate everything
//TODO 6. Change app package name
//TODO 7. Sign app https://developer.android.com/studio/publish/app-signing.html

//API KEY: AIzaSyDATTRTDPHaQGVFTDBaZPO4B2ImXFBa278
//Matrix API: AIzaSyDauzJaBALQg5ZTgS01X7TsjJ7A08MdEM4


public class Home extends AppCompatActivity {

    public ArrayList<CountryItem> countryItems;
    ListView lv;
    HomeListAdapter adapter;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setupToolbar();
        lv = (ListView) findViewById(R.id.listView);

      /*  navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
               // if(menuItem.isChecked()) menuItem.setChecked(false);
                //else menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                Intent intent;

                switch (menuItem.getItemId()){
                    case R.id.searchCity:
                        intent = new Intent(Home.this,SearchCity.class);
                        startActivity(intent);
                        return true;
                    case R.id.codeMaps:
                        intent = new Intent(Home.this,CodeMaps.class);
                        startActivity(intent);
                        return true;
                    case R.id.about:
                        intent = new Intent(Home.this,About.class);
                        startActivity(intent);
                        return true;
                    default:
                        return true;

                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.app_name,R.string.app_name){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
               clearChecks();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                clearChecks();
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();   */

        new AsyncSetAdapter().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tool_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.searchCity:
                intent = new Intent(Home.this, SearchCity.class);
                startActivity(intent);
                return true;
            case R.id.codeMaps:
                intent = new Intent(Home.this,CodeMaps.class);
                startActivity(intent);
                return true;
            case R.id.about:
                intent = new Intent(Home.this,About.class);
                startActivity(intent);
                return true;
            default:
                return true;

        }
       // return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FCFC03"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.countries));
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(Home.this, R.drawable.ic_arrow_back_yellow_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void clearChecks(){
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }


    public class AsyncSetAdapter extends AsyncTask<String,Integer,String>{

        @Override
        protected String doInBackground(String... params) {
            Values values = new Values(Home.this);
            countryItems = new ArrayList<>(values.generateCountryItems());
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            adapter = new HomeListAdapter(Home.this,countryItems);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(Home.this,Search.class);
                    intent.putExtra("code",countryItems.get(position).getCountry());
                    startActivity(intent);
                    //  finish();
                }
            });
        }
    }
}