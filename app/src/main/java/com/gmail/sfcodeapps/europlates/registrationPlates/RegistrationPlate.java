package com.gmail.sfcodeapps.europlates.registrationPlates;

public class RegistrationPlate {

    private String registration;
    private String city;
    private String country;

    public RegistrationPlate(){
    }

    public RegistrationPlate(String registration, String city, String country){
        this.registration = registration;
        this.city = city;
        this.country = country;
    }

    public void setRegistration(String registration){
        this.registration = registration;
    }

    public void setCity(String city){
        this.city = city;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public String getRegistration(){
        return this.registration;
    }

    public String getCity(){
        return this.city;
    }

    public String getCountry(){
        return this.country;
    }


}

