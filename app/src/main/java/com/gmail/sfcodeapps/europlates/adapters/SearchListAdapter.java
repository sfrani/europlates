package com.gmail.sfcodeapps.europlates.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.gmail.sfcodeapps.europlates.R;
import com.gmail.sfcodeapps.europlates.info.WikipediaInfo;
import com.gmail.sfcodeapps.europlates.maps.MapActivity;

import java.util.ArrayList;

public class SearchListAdapter extends ArrayAdapter<String> {

    Context context;
    ArrayList<String> city;
    ArrayList<String> plate;
    String country;
    ViewHolder holder;
    View rowView;

    public SearchListAdapter(Context context, ArrayList<String> city, ArrayList<String> plate, String country) {
        super(context, R.layout.activity_search_row, city);

        this.context = context;
        this.city = new ArrayList<>(city);
        this.plate = new ArrayList<>(plate);
        this.country = country;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        rowView = view;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.activity_search_row, null, true);
            holder = new ViewHolder();

            holder.txtTitle = (TextView) rowView.findViewById(R.id.textView);
            holder.txtCode = (TextView) rowView.findViewById(R.id.textViewCode);
            holder.imgMap = (ImageButton) rowView.findViewById(R.id.imageButton);
            holder.imgInfo = (ImageButton) rowView.findViewById(R.id.imageButton2);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.txtCode.setText(plate.get(position));
        holder.txtTitle.setText(city.get(position));

        holder.imgMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(context, MapActivity.class);
                map.putExtra("city",city.get(position) + "," + country);
                context.startActivity(map);
            }
        });

        holder.imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(context, WikipediaInfo.class);
                map.putExtra("city",city.get(position));
                context.startActivity(map);
            }
        });

        return rowView;

    }

    static class ViewHolder {
        TextView txtTitle;
        TextView txtCode;
        ImageButton imgMap;
        ImageButton imgInfo;
    }
}
