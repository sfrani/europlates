package com.gmail.sfcodeapps.europlates;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.gmail.sfcodeapps.europlates.adapters.AboutListAdapter;
import com.gmail.sfcodeapps.europlates.helper.Values;

import java.util.ArrayList;

public class About extends AppCompatActivity{

    ListView lv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        lv = (ListView) findViewById(R.id.listViewAbout);
        Values values = new Values(this);
        ArrayList<String> title = new ArrayList<>(values.generateAboutTitles());
        ArrayList<String> description = new ArrayList<>(values.generateAboutDescriptions());
        AboutListAdapter adapter = new AboutListAdapter(this,title,description);
        lv.setAdapter(adapter);
    }
}
