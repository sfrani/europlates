package com.gmail.sfcodeapps.europlates.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gmail.sfcodeapps.europlates.R;
import com.gmail.sfcodeapps.europlates.info.WikipediaInfo;
import com.gmail.sfcodeapps.europlates.maps.MapActivity;
import com.gmail.sfcodeapps.europlates.registrationPlates.RegistrationPlate;

import java.util.ArrayList;

public class SearchCityListAdapter extends ArrayAdapter<RegistrationPlate> {

    Context context;
    ViewHolder holder;
    View rowView;
    ArrayList<RegistrationPlate> rp;

    public SearchCityListAdapter(Context context, ArrayList<RegistrationPlate> rp) {
        super(context, R.layout.activity_search_row, rp);

        this.context = context;
        this.rp = new ArrayList<>(rp);
    }

    public View getView(final int position, View view, ViewGroup parent) {
        rowView = view;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.activity_search_row, null, true);
            holder = new ViewHolder();

            holder.txtTitle = (TextView) rowView.findViewById(R.id.textView);
            holder.txtCode = (TextView) rowView.findViewById(R.id.textViewCode);
            holder.imgMap = (ImageButton) rowView.findViewById(R.id.imageButton);
            holder.imgInfo = (ImageButton) rowView.findViewById(R.id.imageButton2);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        if(rp.size()>position) {
            final RegistrationPlate regInfo = rp.get(position);
            holder.txtCode.setText(regInfo.getRegistration());
            String text = regInfo.getCity() + " , " + regInfo.getCountry();
            holder.txtTitle.setText(text);
        }

        holder.imgMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(context, MapActivity.class);
                map.putExtra("city",rp.get(position).getCity() + "," + rp.get(position).getCountry());
                context.startActivity(map);
            }
        });

        holder.imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(context, WikipediaInfo.class);
                map.putExtra("city",rp.get(position).getCity());
                context.startActivity(map);
            }
        });

        return rowView;

    }

    static class ViewHolder {
        TextView txtTitle;
        TextView txtCode;
        ImageButton imgMap;
        ImageButton imgInfo;
    }
}
