package com.gmail.sfcodeapps.europlates.info;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;

import com.gmail.sfcodeapps.europlates.R;
import com.gmail.sfcodeapps.europlates.helper.MyWebClient;

import java.util.Locale;

//import cn.pedant.SweetAlert.SweetAlertDialog;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

public class WikipediaInfo extends AppCompatActivity implements MyWebClient.WebListener {

    Toolbar toolbar;
    String city;
    SweetAlertDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wikipediainfo);

        city = getIntent().getExtras().getString("city");
        Log.d("AlarmLOG","wiki search= " + city);

        if(city.contains("(")){
            city = city.substring(0,city.indexOf("("));
        }

        setupToolbar();
        setupProgressDialog();

        WebView webView = (WebView) findViewById(R.id.webView);
       // webView.setWebViewClient(new WebViewClient());
        webView.setWebViewClient(new MyWebClient(this));

        String language = Locale.getDefault().getLanguage();
        switch(language){
            case "pl":
                webView.loadUrl("https://pl.wikipedia.org/wiki/" + city);
                break;
            case "hr":
                webView.loadUrl("https://hr.wikipedia.org/wiki/" + city);
                break;
            case "sr":
                webView.loadUrl("https://rs.wikipedia.org/wiki/" + city);
                break;
            case "it":
                webView.loadUrl("https://it.wikipedia.org/wiki/" + city);
                break;
            case "es":
                webView.loadUrl("https://es.wikipedia.org/wiki/" + city);
                break;
            case "de":
                webView.loadUrl("https://de.wikipedia.org/wiki/" + city);
                break;
            default:
                webView.loadUrl("https://en.wikipedia.org/wiki/" + city);
                break;
        }
    }


    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FCFC03"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(city);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(WikipediaInfo.this, R.drawable.ic_arrow_back_yellow_24dp));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPageLoaded() {
        if(pDialog != null){
            pDialog.dismissWithAnimation();
        }
    }

    private void setupProgressDialog(){
        pDialog = new SweetAlertDialog(WikipediaInfo.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#4DB6AC"));
        pDialog.getProgressHelper().setCircleRadius(40);
        pDialog.setTitleText(WikipediaInfo.this.getString(R.string.please_wait));
        pDialog.setCancelable(true);
        pDialog.show();
    }
}
