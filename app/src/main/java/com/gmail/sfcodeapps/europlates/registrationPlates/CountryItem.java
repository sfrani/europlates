package com.gmail.sfcodeapps.europlates.registrationPlates;

public class CountryItem {

    String title;
    int image;
    int plate;
    String country;

    public CountryItem(String title, int image, int plate, String country){
        this.title = title;
        this.image = image;
        this.plate = plate;
        this.country = country;
    }

    public String getTitle(){
        return title;
    }

    public int getImage(){
        return image;
    }

    public int getPlate(){
        return plate;
    }

    public String getCountry(){
        return country;
    }
}
