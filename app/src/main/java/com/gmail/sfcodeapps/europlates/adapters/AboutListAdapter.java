package com.gmail.sfcodeapps.europlates.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.sfcodeapps.europlates.R;
import java.util.ArrayList;

public class AboutListAdapter extends ArrayAdapter<String> {

    Context context;
    ArrayList<String> title;
    ArrayList<String> description;
    ViewHolder holder;
    View rowView;

    public AboutListAdapter(Context context, ArrayList<String> title, ArrayList<String> description) {
        super(context, R.layout.activity_about_row, title);

        this.context = context;
        this.title = new ArrayList<>(title);
        this.description = new ArrayList<>(description);
    }

    public View getView(final int position, View view, ViewGroup parent) {
        rowView = view;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(position==5){
                rowView = inflater.inflate(R.layout.rate_row, null, true);
            } else {
                rowView = inflater.inflate(R.layout.activity_about_row, null, true);
            }
            holder = new ViewHolder();

            holder.txtTitle = (TextView) rowView.findViewById(R.id.textViewTitle);
            holder.txtDescription = (TextView) rowView.findViewById(R.id.textViewDescription);
            if(position == 5){
                holder.btnRate = (Button) rowView.findViewById(R.id.buttonRate);
                holder.btnRate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        launchMarket(context);
                    }
                });
            }

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.txtTitle.setText(title.get(position));
        holder.txtDescription.setText(description.get(position));

        return rowView;

    }

    static class ViewHolder {
        TextView txtTitle;
        TextView txtDescription;
        Button btnRate;
    }

    private void launchMarket(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            context.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context,context.getString(R.string.rate_error), Toast.LENGTH_LONG).show();
        }
    }
}
