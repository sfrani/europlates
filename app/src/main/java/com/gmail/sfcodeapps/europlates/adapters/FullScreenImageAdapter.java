package com.gmail.sfcodeapps.europlates.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gmail.sfcodeapps.europlates.R;
import com.gmail.sfcodeapps.europlates.helper.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class FullScreenImageAdapter extends PagerAdapter {

    private Activity activity;
    private ArrayList<Integer> data = new ArrayList<>();
    ArrayList<String> country = new ArrayList<>();

    public FullScreenImageAdapter(Activity activity,
                                  ArrayList<Integer> data, ArrayList<String> country) {
        this.activity = activity;
        this.data = data;
        this.country = country;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;

        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.activity_codemaps, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.touchImageView);
        TextView tvText = (TextView) viewLayout.findViewById(R.id.tvCountry);
        tvText.setText(country.get(position));
        Picasso.with(activity).load(data.get(position)).into(imgDisplay);
        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);

    }

}
