package com.gmail.sfcodeapps.europlates;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.gmail.sfcodeapps.europlates.adapters.SearchListAdapter;
import com.gmail.sfcodeapps.europlates.database.DatabaseHandler;
import com.gmail.sfcodeapps.europlates.database.PlateDB;
import com.gmail.sfcodeapps.europlates.helper.Values;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Search extends AppCompatActivity {

    private EditText et;
    private ListView lv;
    private SearchListAdapter adapter;
    private ArrayList<String> city;
    private ArrayList<String> plate;
    private ArrayList<String> cityStart;
    private ArrayList<String> plateStart;
    private HashMap<String, String> countries;
    private HashMap<String, String> importantCities;
    private String country;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Values values = new Values(this);
        countries = new HashMap<>(values.generateCountries());
        importantCities = new HashMap<>(values.generateImportantCities());

        final String code = getIntent().getStringExtra("code");
        country = countries.get(code);

        setupToolbar();

        et = (EditText) findViewById(R.id.editText);
        lv = (ListView) findViewById(R.id.listView2);

        switch (code){
            case "CZ": et.setHint(getString(R.string.enter_cz));
                break;
            case "RUS": et.setHint(getString(R.string.enter_rus));
                break;
            case "I": et.setHint(getString(R.string.enter_rus));
                break;
            default: et.setHint(getString(R.string.enter_plate));
                break;
        }

        DatabaseHandler db = new DatabaseHandler(this);
        final List<PlateDB> plateDBs = db.getAllPlates();

        cityStart = new ArrayList<>();
        plateStart = new ArrayList<>();
        for (PlateDB pdb : plateDBs) {
            if (pdb.getCountry().equals(code)) {
                String cityName;
                if(pdb.getCity().contains("(district)")){
                    cityName = pdb.getCity().replace("district",getString(R.string.district));
                } else {
                     cityName = pdb.getCity();
                }
                if (importantCities.containsKey(pdb.getCity())) {
                    cityStart.add(importantCities.get(pdb.getCity()));
                } else {
                    cityStart.add(cityName);
                }
              /*  if (importantCities.containsKey(pdb.getCity())) {
                    cityStart.add(importantCities.get(pdb.getCity()));
                } else {
                    cityStart.add(pdb.getCity());
                }   */
                // city.add(pdb.getCity());
                plateStart.add(pdb.getPlate());
                adapter = new SearchListAdapter(Search.this, cityStart, plateStart, country);
                lv.setAdapter(adapter);
            }
        }

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // Log.d("AlarmLOG","sequence" + s);
                city = new ArrayList<>();
                plate = new ArrayList<>();
                SearchListAdapter adapter = new SearchListAdapter(Search.this, city, plate, country);
                lv.setAdapter(adapter);
                if (s.length() > 0) {
                    if (code.equals("CZ")) {
                        for (PlateDB pdb : plateDBs) {
                            if (s.length() >= 2) {
                                if (pdb.getPlate().equals(Character.toString(s.charAt(1)).toUpperCase()) && pdb.getCountry().equals(code)) {
                                    refreshAdapter(pdb.getCity(),pdb.getPlate());
                                }
                            }
                        }
                    } else {
                        for (PlateDB pdb : plateDBs) {
                            if(s.length()<=2){
                                  if (pdb.getPlate().startsWith(s.toString().toUpperCase()) && pdb.getCountry().equals(code)) {
                                      refreshAdapter(pdb.getCity(),pdb.getPlate());
                                  }
                            } else {
                                if (s.toString().toUpperCase().startsWith(pdb.getPlate()) && pdb.getCountry().equals(code)) {
                                    refreshAdapter(pdb.getCity(), pdb.getPlate());
                                }
                            }
                        }
                    }


                } else {
                    adapter = new SearchListAdapter(Search.this, cityStart, plateStart, country);
                    lv.setAdapter(adapter);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FCFC03"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(country);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(Search.this, R.drawable.ic_arrow_back_yellow_24dp));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
              /*  Intent intent = new Intent(Search.this,Home.class);
                startActivity(intent);
                finish(); */
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void refreshAdapter(String cityName, String plateCode) {
        String newName;
        if(cityName.contains("(district)")){
            newName = cityName.replace("district",getString(R.string.district));
        } else{
            newName = cityName;
        }
        if (importantCities.containsKey(cityName)) {
            city.add(importantCities.get(cityName));
        } else {
            city.add(newName);
        }
       /* if (importantCities.containsKey(cityName)) {
            city.add(importantCities.get(cityName));
        } else {
            city.add(cityName);
        }  */
        plate.add(plateCode);
        SearchListAdapter adapter = new SearchListAdapter(Search.this, city, plate, country);
        lv.setAdapter(adapter);
    }

}
