package com.gmail.sfcodeapps.europlates;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.gmail.sfcodeapps.europlates.adapters.SearchCityListAdapter;
import com.gmail.sfcodeapps.europlates.database.DatabaseHandler;
import com.gmail.sfcodeapps.europlates.database.PlateDB;
import com.gmail.sfcodeapps.europlates.helper.Values;
import com.gmail.sfcodeapps.europlates.registrationPlates.RegistrationPlate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchCity extends AppCompatActivity {

    private EditText et;
    private ListView lv;
    private ArrayList<RegistrationPlate> rp;
    private HashMap<String, String> countries;
    private HashMap<String, String> importantCities;
    private Toolbar toolbar;
    private SearchCityListAdapter adapter;
    private HashMap<String,String> importantReverse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Values values = new Values(this);
        countries = new HashMap<>(values.generateCountries());
        importantCities = new HashMap<>(values.generateImportantCities());

        setupToolbar();

        et = (EditText) findViewById(R.id.editText);
        lv = (ListView) findViewById(R.id.listView2);

        importantReverse = new HashMap<>();
        for(Map.Entry<String, String> entry : importantCities.entrySet()){
            importantReverse.put(entry.getValue(), entry.getKey());
        }

        DatabaseHandler db = new DatabaseHandler(this);
        final List<PlateDB> plateDBs = db.getAllPlates();

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rp = new ArrayList<>();
                adapter = new SearchCityListAdapter(SearchCity.this, rp);
                lv.setAdapter(adapter);
                if(s.length()>0) {
                    for (PlateDB pdb : plateDBs) {
                        if(pdb.getCity().toUpperCase().startsWith(s.toString().toUpperCase()) && pdb.getCity().length() >= s.toString().length()){
                            Log.d("DataLOG","adding " + pdb.getCity() );
                            String cityName;
                            if(pdb.getCity().contains("(district)")){
                                cityName = pdb.getCity().replace("district",getString(R.string.district));
                            } else {
                                cityName = pdb.getCity();
                            }
                            if(!importantCities.containsKey(pdb.getCity())){
                                rp.add(new RegistrationPlate(pdb.getPlate(),cityName,countries.get(pdb.getCountry())));
                            }
                            adapter = new SearchCityListAdapter(SearchCity.this, rp);
                            lv.setAdapter(adapter);
                        }
                    }
                    for (Map.Entry<String,String> entry : importantCities.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        if(value.toUpperCase().startsWith(s.toString().toUpperCase()) && value.length() >= s.toString().length()){
                            for (PlateDB pdb : plateDBs) {
                                if(pdb.getCity().equals(key)){
                                    rp.add(new RegistrationPlate(pdb.getPlate(),value,countries.get(pdb.getCountry())));
                                    adapter = new SearchCityListAdapter(SearchCity.this, rp);
                                    lv.setAdapter(adapter);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FCFC03"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Search for city");
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(SearchCity.this, R.drawable.ic_arrow_back_yellow_24dp));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
