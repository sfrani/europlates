package com.gmail.sfcodeapps.europlates.database;

public class PlateDB {

    int id;
    String country;
    String plate;
    String city;

    public PlateDB(){

    }

    public PlateDB(String country, String plate, String city){
        this.country = country;
        this.plate = plate;
        this.city = city;
    }

    public int getID(){
        return id;
    }

    public void setID(int ID){
        this.id = ID;
    }

    public String getCountry(){
        return country;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public String getPlate(){
        return plate;
    }

    public void setPlate(String plate){
        this.plate = plate;
    }

    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city = city;
    }

}
