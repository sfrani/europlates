package com.gmail.sfcodeapps.europlates;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.gmail.sfcodeapps.europlates.database.DatabaseHandler;
import com.gmail.sfcodeapps.europlates.helper.ProgressChangeEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

//import cn.pedant.SweetAlert.SweetAlertDialog;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

public class Homescreen extends AppCompatActivity {

    Button btnRecognize;
    Button btnChoose;
    public static final String PREFS_NAME = "REGISTRATION_PREFS";
    SweetAlertDialog pDialog;
    private static boolean serviceRunning = false;
    private static boolean needForDialog = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        EventBus.getDefault().register(this);

        btnRecognize = (Button) findViewById(R.id.buttonRecognize);
        btnChoose = (Button) findViewById(R.id.buttonChoose);

        btnRecognize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homescreen.this,PlateRecognition.class);
                startActivity(intent);
            }
        });

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homescreen.this,Home.class);
              //  Intent intent = new Intent(Homescreen.this,NavActivity.class);
                startActivity(intent);
            }
        });


        if(!serviceRunning) {
            DatabaseHandler db = new DatabaseHandler(this);
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            boolean doneLoading = settings.getBoolean("finished", false);
            if (db.getAllPlates().size() == 0 || !doneLoading) {
                    setupProgressDialog();
                    Intent intent = new Intent(this,FillService.class);
                    startService(intent);
                    serviceRunning = true;
                    needForDialog = true;
            }
        }
        else{
            if(needForDialog) {
                setupProgressDialog();
            }
        }

    }

    private void setupProgressDialog(){
        pDialog = new SweetAlertDialog(Homescreen.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#4DB6AC"));
      //  pDialog.getProgressHelper().setBarColor(Color.parseColor());
        pDialog.getProgressHelper().setCircleRadius(40);
        pDialog.setTitleText(Homescreen.this.getString(R.string.loading_data));
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Subscribe
    public void onProgressChange(final ProgressChangeEvent event) {
        Log.d("ProgressLOG","progress= " + event.getProgress());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pDialog != null) {
                    pDialog.setTitleText(Homescreen.this.getString(R.string.loading_data) + event.getProgress() + "%");
                    if (event.getProgress() == 100) {
                        pDialog.dismissWithAnimation();
                        needForDialog = false;
                    }
                }
            }
        });
    }

}
