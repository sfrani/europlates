package com.gmail.sfcodeapps.europlates;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import com.gmail.sfcodeapps.europlates.adapters.FullScreenImageAdapter;
import com.gmail.sfcodeapps.europlates.helper.Values;

import java.util.ArrayList;

public class CodeMaps extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codemaps_viewpager);

        Values values = new Values(this);
        ArrayList<Integer> items = new ArrayList<>(values.generateCodeMaps());
        ArrayList<String> countries = new ArrayList<>(values.generateCodeMapsCountries());
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        FullScreenImageAdapter adapter = new FullScreenImageAdapter(this,
                items, countries);

        viewPager.setAdapter(adapter);
    }
}
