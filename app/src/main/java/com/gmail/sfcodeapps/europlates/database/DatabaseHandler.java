package com.gmail.sfcodeapps.europlates.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "PlatesManager";
    private static final String TABLE_REGISTRATION_PLATES = "RegistrationPlates";
    private static final String KEY_ID = "id";
    private static final String KEY_CITY = "city";
    private static final String KEY_PLATE = "plate";
    private static final String KEY_COUNTRY = "country";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_REGISTRATION_PLATES_TABLE = "CREATE TABLE " + TABLE_REGISTRATION_PLATES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_COUNTRY + " TEXT,"
                + KEY_PLATE + " TEXT," + KEY_CITY + " TEXT" + ")";
        db.execSQL(CREATE_REGISTRATION_PLATES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGISTRATION_PLATES);
        onCreate(db);
    }


    public void addPlate(PlateDB plateDB) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_COUNTRY, plateDB.getCountry());
        values.put(KEY_PLATE, plateDB.getPlate());
        values.put(KEY_CITY, plateDB.getCity());

        db.insert(TABLE_REGISTRATION_PLATES, null, values);
        db.close();
    }


    public List<PlateDB> getAllPlates() {
        List<PlateDB> contactList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_REGISTRATION_PLATES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                PlateDB plateDB = new PlateDB();
                plateDB.setID(Integer.parseInt(cursor.getString(0)));
                plateDB.setCountry(cursor.getString(1));
                plateDB.setPlate(cursor.getString(2));
                plateDB.setCity(cursor.getString(3));
                contactList.add(plateDB);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return contactList;
    }


    public void deletePlate(PlateDB plateDB) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REGISTRATION_PLATES, KEY_ID + " = ?",
                new String[]{String.valueOf(plateDB.getID())});
        db.close();
    }
}

