package com.gmail.sfcodeapps.europlates.helper;

public class ProgressChangeEvent {

    public int progress;

    public ProgressChangeEvent(int progress){
        this.progress = progress;
    }

    public int getProgress(){
        return progress;
    }
}
