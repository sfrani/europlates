package com.gmail.sfcodeapps.europlates;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.gmail.sfcodeapps.europlates.database.DatabaseHandler;
import com.gmail.sfcodeapps.europlates.database.PlateDB;
import com.gmail.sfcodeapps.europlates.helper.ProgressChangeEvent;
import com.gmail.sfcodeapps.europlates.registrationPlates.RegistrationPlate;

import org.greenrobot.eventbus.EventBus;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FillService extends Service {

    RegistrationPlate plate;
    ArrayList<RegistrationPlate> registrationPlates = new ArrayList<>();
    int event;
    String text = null;
    public XmlPullParserFactory xmlFactoryObject;
    public XmlPullParser myparser;
    public static final String PREFS_NAME = "REGISTRATION_PREFS";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new AsyncFill().execute();
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class AsyncFill extends AsyncTask<String,Integer,String>{
        @Override
        protected String doInBackground(String... params) {

            DatabaseHandler db = new DatabaseHandler(FillService.this);
            if (db.getAllPlates().size() > 0) {
                List<PlateDB> plateDBs = db.getAllPlates();
                for (PlateDB pdb : plateDBs) {
                    db.deletePlate(pdb);
                }
            }

            try {
                xmlFactoryObject = XmlPullParserFactory.newInstance();
                myparser = xmlFactoryObject.newPullParser();
                InputStream stream = getApplicationContext().getResources().openRawResource(R.raw.data);
                myparser.setInput(stream, null);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                event = myparser.getEventType();

                while (event != XmlPullParser.END_DOCUMENT) {
                    String name = myparser.getName();

                    switch (event) {
                        case XmlPullParser.START_TAG:
                            if (name.equalsIgnoreCase("plate")) {
                                //  city.add(text);
                                plate = new RegistrationPlate();
                            }
                            break;

                        case XmlPullParser.TEXT:
                            text = myparser.getText();
                            break;

                        case XmlPullParser.END_TAG:
                            if (name.equalsIgnoreCase("plate")) {
                                registrationPlates.add(plate);
                            }
                            if (name.equalsIgnoreCase("city")) {
                                plate.setCity(text);
                            }
                            if (name.equalsIgnoreCase("registration")) {
                                plate.setRegistration(text);
                            }
                            if (name.equalsIgnoreCase("country")) {
                                plate.setCountry(text);
                            }
                            break;

                        default:
                            break;
                    }
                    event = myparser.next();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("AlarmLOG","" + e);
            }

            Log.d("AlarmLOG", "size= " + registrationPlates.size());
          //  DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            for (int i = 0; i < registrationPlates.size(); i++) {
              //  Log.d("AlarmLOG", "country=" + registrationPlates.get(i).getCountry() + " , registration= " + registrationPlates.get(i).getRegistration() + " , city= " + registrationPlates.get(i).getCity());
                db.addPlate(new PlateDB(registrationPlates.get(i).getCountry() , registrationPlates.get(i).getRegistration(), registrationPlates.get(i).getCity()));
                float progress = ((float)i/registrationPlates.size())*100;
                EventBus.getDefault().post(new ProgressChangeEvent((int) progress));
            }
            SharedPreferences settings = getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            settings.edit().putBoolean("finished",true).apply();
            EventBus.getDefault().post(new ProgressChangeEvent(100));
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            FillService.this.stopSelf();
        }
    }
}
