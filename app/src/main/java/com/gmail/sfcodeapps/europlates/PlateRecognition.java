package com.gmail.sfcodeapps.europlates;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gmail.sfcodeapps.europlates.adapters.SearchCityListAdapter;
import com.gmail.sfcodeapps.europlates.database.DatabaseHandler;
import com.gmail.sfcodeapps.europlates.database.PlateDB;
import com.gmail.sfcodeapps.europlates.helper.Values;
import com.gmail.sfcodeapps.europlates.registrationPlates.RegistrationPlate;
import com.google.gson.Gson;

import org.openalpr.OpenALPR;
import org.openalpr.model.Results;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import cn.pedant.SweetAlert.SweetAlertDialog;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class PlateRecognition extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    ImageButton takePicture;
    ImageView iv;
    TextView tvPlate;
    ListView lv;
    private static int TAKE_PICTURE = 202;
    private String mCurrentPhotoPath;
    private long photoTime;
    private final int RC_CAMERA_AND_STORAGE = 111;
    private SearchCityListAdapter adapter;
    ArrayList<RegistrationPlate> rp;
    HashMap<String,String> importantCities;
    HashMap<String,String> countries;
    private final String FOLDER_PATH = Environment.getExternalStorageDirectory() + File.separator + "EuroPlates";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_platerecognition);

        checkForPermission();

        showMessage();

        takePicture = (ImageButton) findViewById(R.id.buttonTakePicture);
        iv = (ImageView) findViewById(R.id.imageViewPlate);
        tvPlate = (TextView) findViewById(R.id.textView2);
        lv = (ListView) findViewById(R.id.listView3);

        Values values = new Values(this);
        importantCities = new HashMap<>(values.generateImportantCities());
        countries = new HashMap<>(values.generateCountries());


        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
                if (tvPlate != null) {
                    tvPlate.setText("");
                }
                if (lv != null && adapter!=null) {
                    adapter.clear();
                }
            }
        });
    }

    @AfterPermissionGranted(RC_CAMERA_AND_STORAGE)
    private void checkForPermission() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.permissions_needed),
                    RC_CAMERA_AND_STORAGE, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        finish();
    }


    private void showMessage(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PlateRecognition.this,R.style.MessageDialog);
        builder1.setMessage(getString(R.string.how_to_use));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void dispatchTakePictureIntent() {
        photoTime = System.currentTimeMillis();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, TAKE_PICTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH);
        String currentDateandTime = sdf.format(new Date()).replace(" ", "");
        String imageFileName = currentDateandTime + ".jpg";
        createFolder();
        File image = new File(FOLDER_PATH + File.separator + imageFileName);
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("ImageLOG", "path= " + image.getAbsolutePath());
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == TAKE_PICTURE) {
                iv.setVisibility(View.VISIBLE);
                tvPlate.setVisibility(View.VISIBLE);
                File f = new File(mCurrentPhotoPath);

                removeLastFromGallery();
                Glide.with(PlateRecognition.this).load(f.getAbsolutePath()).override(800, 200).fitCenter().into(iv);
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(f)));
                new AsyncRecognize().execute(f.getAbsolutePath());
            }
        }

    }


    private void removeLastFromGallery() {
        String[] projection = {MediaStore.Images.ImageColumns.SIZE,
                MediaStore.Images.ImageColumns.DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATA,
                BaseColumns._ID, MediaStore.Images.ImageColumns.DATE_ADDED};
        final String imageOrderBy = MediaStore.Images.Media._ID + " DESC";
        final String selection = MediaStore.Images.Media.DATE_TAKEN + " > " + photoTime;
        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor c = PlateRecognition.this.getContentResolver().query(u, projection, selection, null, imageOrderBy);
        if (null != c && c.moveToFirst()) {
            ContentResolver cr = PlateRecognition.this.getContentResolver();
            cr.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    BaseColumns._ID + "=" + c.getString(3), null);
        }
        if (c != null) {
            c.close();
        }
    }


    public class AsyncRecognize extends AsyncTask<String, Integer, String> {

        String result;
        SweetAlertDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new SweetAlertDialog(PlateRecognition.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#4DB6AC"));
            pDialog.getProgressHelper().setCircleRadius(40);
            pDialog.setTitleText(PlateRecognition.this.getString(R.string.please_wait));
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
           // final String ANDROID_DATA_DIR = "/data/data/com.gmail.sfcodeapps.registrationplatesofeurope";
            final String ANDROID_DATA_DIR = "/data/data/com.gmail.sfcodeapps.europlates";
          //  final String ANDROID_DATA_DIR = PlateRecognition.this.getFilesDir().getPath();
            Log.d("AlarmLOG", "data dir= " + ANDROID_DATA_DIR);
            final String openAlprConfFile = ANDROID_DATA_DIR + File.separatorChar + "runtime_data" + File.separatorChar + "openalpr.conf";
            result = OpenALPR.Factory.create(PlateRecognition.this, ANDROID_DATA_DIR).recognizeWithCountryRegionNConfig("eu", "", params[0], openAlprConfFile, 2);
          //  result = OpenALPR.Factory.create(PlateRecognition.this, ANDROID_DATA_DIR).recognizeWithCountryRegionNConfig("eu", "pl", params[0], openAlprConfFile, 10);
          //  result = OpenALPR.Factory.create(PlateRecognition.this,ANDROID_DATA_DIR).recognize(params[0],2);
            Log.d("AlarmLOG", "result= " + result);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            final Results results = new Gson().fromJson(result, Results.class);
            if (results == null || results.getResults() == null || results.getResults().size() == 0) {
                tvPlate.setText(getString(R.string.cant_recognize));
            } else {
                tvPlate.setText(results.getResults().get(0).getPlate());
                //  }

                int count = 0;
                if (results.getResults().get(0).getPlate().length() >= 3) {
                    for (int i = 0; i < 3; i++) {
                        if (Character.isLetter(results.getResults().get(0).getPlate().charAt(i))) {
                            count++;
                        }
                    }
                }

                boolean fitsCzech = false;
                if (results.getResults().get(0).getPlate().length() >= 2) {
                        if (Character.isDigit(results.getResults().get(0).getPlate().charAt(0)) && Character.isLetter(results.getResults().get(0).getPlate().charAt(1))) {
                            fitsCzech = true;
                        }
                }

                Log.d("AlarmLOG", "count= " + count);
                Log.d("AlarmLOG", results.getResults().get(0).getPlate());
                DatabaseHandler db = new DatabaseHandler(PlateRecognition.this);
                final List<PlateDB> plateDBs = db.getAllPlates();
                 rp = new ArrayList<>();

                Log.d("PlateLOG","fitsCzech= " + fitsCzech);
                if(!fitsCzech){
                    for (PlateDB pdb : plateDBs) {
                        if (pdb.getPlate().length() == count && results.getResults().get(0).getPlate().startsWith(pdb.getPlate())
                                && !pdb.getCountry().equals("CZ") && !pdb.getCountry().equals("RUS")) {
                            setAdapter(pdb.getPlate(),pdb.getCity(),pdb.getCountry());
                        }
                    }
                } else{
                    for (PlateDB pdb : plateDBs) {
                        if(pdb.getCountry().equals("CZ") && pdb.getPlate().charAt(0) == results.getResults().get(0).getPlate().toUpperCase().charAt(1)){
                            setAdapter(pdb.getPlate(),pdb.getCity(),pdb.getCountry());
                        }
                    }
                }

            }
          //  progress.dismiss();
            pDialog.dismissWithAnimation();
           // removeLastFromGallery();
        }
    }


    private void setAdapter(String plate, String city, String country){
        String newName;
        if(city.contains("(district)")){
            newName = city.replace("district",getString(R.string.district));
        } else {
            newName = city;
        }
        if (importantCities.containsKey(city)) {
            rp.add(new RegistrationPlate(plate,importantCities.get(city),countries.get(country)));
        } else {
            rp.add(new RegistrationPlate(plate,newName,countries.get(country)));
        }
        adapter = new SearchCityListAdapter(PlateRecognition.this, rp);
        lv.setAdapter(adapter);
    }

    private void createFolder(){
        File folder = new File(FOLDER_PATH);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
    }

}
