package com.gmail.sfcodeapps.europlates.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.sfcodeapps.europlates.R;
import com.gmail.sfcodeapps.europlates.registrationPlates.CountryItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeListAdapter extends ArrayAdapter<CountryItem> {

    Context context;
    ArrayList<CountryItem> countryItems;
    ViewHolder holder;
    View rowView;

    public HomeListAdapter(Context context, ArrayList<CountryItem> countryItems) {
        super(context, R.layout.activity_home_row, countryItems);

        this.context = context;
        this.countryItems = new ArrayList<>(countryItems);
    }

    public View getView(int position, View view, ViewGroup parent) {
        rowView = view;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.activity_home_row, null, true);
            holder = new ViewHolder();

            holder.txtTitle = (TextView) rowView.findViewById(R.id.textView);
            holder.imageView = (ImageView) rowView.findViewById(R.id.imageView);
            holder.imageView2 = (ImageView) rowView.findViewById(R.id.imageView2);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.txtTitle.setText(countryItems.get(position).getTitle());
        Picasso.with(context).load(countryItems.get(position).getImage()).resize(80, 50).into(holder.imageView);
        Picasso.with(context).load(countryItems.get(position).getPlate()).resize(120, 40).into(holder.imageView2);
        return rowView;

    }

    static class ViewHolder {
        TextView txtTitle;
        ImageView imageView;
        ImageView imageView2;
    }
}
