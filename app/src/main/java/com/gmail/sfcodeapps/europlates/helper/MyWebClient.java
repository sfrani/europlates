package com.gmail.sfcodeapps.europlates.helper;

import android.app.Activity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebClient extends WebViewClient {

    public interface WebListener{
        void onPageLoaded();
    }

    Activity activity;

    public MyWebClient(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if(activity != null) {
            WebListener listener = (WebListener) activity;
            listener.onPageLoaded();
        }
    }
}
